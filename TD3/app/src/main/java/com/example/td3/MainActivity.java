package com.example.td3;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ArrayList<JeuVideo> mesJeux = new ArrayList<JeuVideo>();
        mesJeux.add(new JeuVideo("Bioshock","90"));
        mesJeux.add(new JeuVideo("Red Dead Redemption","30"));
        mesJeux.add(new JeuVideo("GTA V","20"));

        RecyclerView myRecyclerView = findViewById(R.id.myRecyclerView);
//        myRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        myRecyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false));
//        myRecyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false));
//        myRecyclerView.setLayoutManager(new GridLayoutManager(getApplicationContext(), 3));
        myRecyclerView.setAdapter( new MyVideoGamesAdapter(mesJeux));
    }
}
