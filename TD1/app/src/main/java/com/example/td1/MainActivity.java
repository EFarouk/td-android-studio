package com.example.td1;

import android.graphics.Color;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    private EditText etGauche;
    private EditText etDroit;
    private TextView result;
    private String symbole;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        Button button = findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switch (symbole) {
                    case "+":
                        computeAdd(view);
                        break;
                    case "-":
                        computeSubstract(view);
                        break;
                    case "*":
                        computeMutiply(view);
                        break;
                    case "/":
                        computeDivide(view);
                        break;
                }
            }
        });

        final Button divideButton = findViewById(R.id.divideButton);
        divideButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                symbole = "/";
                
            }
        });

        Button addButton = findViewById(R.id.addButton);
        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                symbole = "+";
            }
        });

        Button substractButton = findViewById(R.id.substractButton);
        substractButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                symbole = "-";
            }
        });

        Button mutiplyButton = findViewById(R.id.mutiplyButton);
        mutiplyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                symbole = "*";
            }
        });

        etGauche = (EditText) findViewById(R.id.etGauche);
        etDroit = (EditText) findViewById(R.id.etDroite);
        result = (TextView) findViewById(R.id.result);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void computeAdd(View v) {

        if (etDroit.getText().toString().equals("") || etGauche.getText().toString().equals("")) {
            result.setText("Un ou plusieurs champs ne sont pas renseignés");
        } else {
            int a = Integer.parseInt(etDroit.getText().toString());
            int b = Integer.parseInt(etGauche.getText().toString());
            int c = a + b;
            result.setText((String.valueOf(c)));
        }
    }

    public void computeMutiply(View v) {

        if (etDroit.getText().toString().equals("") || etGauche.getText().toString().equals("")) {
            result.setText("Un ou plusieurs champs ne sont pas renseignés");
        } else {
            int a = Integer.parseInt(etDroit.getText().toString());
            int b = Integer.parseInt(etGauche.getText().toString());
            int c = a * b;
            result.setText((String.valueOf(c)));
        }
    }

    public void computeDivide(View v) {

        if (etDroit.getText().toString().equals("") || etGauche.getText().toString().equals("")) {
            result.setText("Un ou plusieurs champs ne sont pas renseignés");
        } else {
            if (etGauche.getText().toString().equals("0")) {
                result.setText("La division par zéro n'est pas autorisée");
            } else {
                int a = Integer.parseInt(etDroit.getText().toString());
                int b = Integer.parseInt(etGauche.getText().toString());
                int c = Math.abs(a/b);
                result.setText((String.valueOf(c)));
            }
        }
    }

    public void computeSubstract(View v) {

        if (etDroit.getText().toString().equals("") || etGauche.getText().toString().equals("")) {
            result.setText("Un ou plusieurs champs ne sont pas renseignés");
        } else {

            int a = Integer.parseInt(etDroit.getText().toString());
            int b = Integer.parseInt(etGauche.getText().toString());
            int c = a - b;
            result.setText((String.valueOf(c)));
        }
    }
}
